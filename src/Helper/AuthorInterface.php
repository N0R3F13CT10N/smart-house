<?php
/**
 * Created by PhpStorm.
 * User: sergey
 * Date: 15/07/2019
 * Time: 18:03
 */

namespace App\Helper;


use Symfony\Component\Security\Core\User\UserInterface;

interface AuthorInterface
{
    public function getAuthor(): ?UserInterface;
    public function setAuthor(UserInterface $user);
}
