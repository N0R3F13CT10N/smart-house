<?php
/**
 * Created by PhpStorm.
 * User: sergey
 * Date: 15/07/2019
 * Time: 18:06
 */

namespace App\Helper;


use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Security\Core\User\UserInterface;
use Symfony\Component\Serializer\Annotation\Groups;

trait AuthorTrait
{
    /**
     * @var UserInterface
     * @ORM\ManyToOne(targetEntity="App\Entity\User")
     * @Groups({"author_show"})
     */
    protected $author;

    /**
     * @return UserInterface
     */
    public function getAuthor(): ?UserInterface
    {
        return $this->author;
    }

    /**
     * @param UserInterface $author
     */
    public function setAuthor(UserInterface $author): void
    {
        $this->author = $author;
    }
}
