<?php

namespace App\Controller;

use App\Entity\DeviceType;
use App\Form\DeviceTypeType;
use App\Repository\DeviceTypeRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/device_type")
 */
class DeviceTypeController extends AbstractController
{
    /**
     * @Route("/", name="device_type_index", methods={"GET"})
     */
    public function index(DeviceTypeRepository $deviceTypeRepository): Response
    {
        return $this->render('device_type/index.html.twig', [
            'device_types' => $deviceTypeRepository->findAll(),
        ]);
    }

    /**
     * @Route("/new", name="device_type_new", methods={"GET","POST"})
     */
    public function new(Request $request): Response
    {
        $deviceType = new DeviceType();
        $form = $this->createForm(DeviceTypeType::class, $deviceType);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($deviceType);
            $entityManager->flush();

            return $this->redirectToRoute('device_type_index');
        }

        return $this->render('device_type/new.html.twig', [
            'device_type' => $deviceType,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/{id}", name="device_type_show", methods={"GET"})
     */
    public function show(DeviceType $deviceType): Response
    {
        return $this->render('device_type/show.html.twig', [
            'device_type' => $deviceType,
        ]);
    }

    /**
     * @Route("/{id}/edit", name="device_type_edit", methods={"GET","POST"})
     */
    public function edit(Request $request, DeviceType $deviceType): Response
    {
        $form = $this->createForm(DeviceTypeType::class, $deviceType);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $this->getDoctrine()->getManager()->flush();

            return $this->redirectToRoute('device_type_index');
        }

        return $this->render('device_type/edit.html.twig', [
            'device_type' => $deviceType,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/{id}", name="device_type_delete", methods={"DELETE"})
     */
    public function delete(Request $request, DeviceType $deviceType): Response
    {
        if ($this->isCsrfTokenValid('delete'.$deviceType->getId(), $request->request->get('_token'))) {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->remove($deviceType);
            $entityManager->flush();
        }

        return $this->redirectToRoute('device_type_index');
    }
}
